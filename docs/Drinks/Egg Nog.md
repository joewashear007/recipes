Egg Nog
==================================================================
*Makes about a 1/2 gallon*

Ingredients
----------------------------------------------------------
* 6 eggs ( Best at room temp)
* 3/4 c. sugar
* 1 pint half & half cream
* 1 pint of whole milk
* rum flavoring to taste (~ 1-1 1/2 tsp.)
* *Optional -  =< 1 pint of blended whiskey*
* *Optional -  1 oz. of Heavy Run*

Directions
---------------------------------------
1. Separate eggs. Beat whites, with 1/4 cup sugar and dash of salt, until stiff
2. Beat egg yolks, with 1/2 cup  sugar, until very stiff.
3. Add whites to yolks and blend
4. Add milk and cream
5. *Optional - Add rum & whiskey.*
6. Chill for at least an hour before serving
7. Top with nutmeg when serving

Notes
-------------------------------------------------
* Originally From Jean Smerklo
* Made During Christmas
