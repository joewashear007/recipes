Old-Fashion Pork Roast
===================================================
*Yield: 8 servings; Prep Time: 15 minutes Cook; Time: 7 hours*

Ingredients:
------------------------------------------------
*   6 lbs. Boneless Pork butt Roast
*   3 garlic cloves minced
*   2 teaspoons peppercorns, cracked
*   1 1/2 teaspoons salt
*   1 teaspoons chopped fresh rosemary
*   1 teaspoons chopped fresh sage
*   2 large red onions, cut into 1 inch wedges
*   1 cup apple cider
*   1/4 cup apple jelly
*   2 tablespoons cider vinegar

Directions:
------------------------------------------------------
1.  Preheat oven to 300° F. Combine garlic, peppercorns, salt, rosemary, and sage. Pat pork dry with paper towel, rub herb over the Roast
2. Bake for 3 hours. Add onion wedges around the pork (add vegetable oil if their is no juices). Cook for another 3-4 hours. Skewer should meet no resistance when inserted.
3. Boil drippings, cider, jelly, and vinegar, reduce to simmer until thickened

Notes:
------------------------------------------------------------------
From Cook's Country TV Show Cookbook Season 8
