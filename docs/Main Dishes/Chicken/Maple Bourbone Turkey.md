Maple Bourbone Turkey
====================================================

Ingredients
-----------------------------------------------------
###Brine:
* 4 quarts water, divided 
* 2 cups dark brown sugar 
* 1 cup soy sauce 
* 1 cup maple syrup 
* 3/4 cup sea salt 
* 8 cloves whole garlic cloves, peeled 
* 6 bay leaves 
* 3 large fresh thyme sprigs 
* 2 teaspoons whole black peppercorns 
* 1 cup Bourbone 

###Rub:
* 1 c Brown Sugar
* 1 Stick Butter
* 1/4 c Maple Syrup
* 1/8 c Bourbone


Directions
-----------------------------------------------
###Brine:
1. Place 2 quarts of water in a large pot over medium heat, and stir in brown sugar, soy sauce, maple syrup, sea salt, garlic cloves, bay leaves, thyme sprigs, peppercorns, and bourbone. 
2. Stir to dissolve brown sugar and salt; bring to a boil. 
3. Remove from heat, and stir in remaining 2 quarts of water.
4. Allow brine to cool completely before using.


###Rub:
1. Combine all ingredients. Rub onto turkey before baking

