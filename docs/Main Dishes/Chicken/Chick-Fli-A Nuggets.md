Copycat Chick-fil-A Nuggets
===================================================
*Yield: 4 servings Prep Time: 50 minutes Cook Time: 10 minutes Total Time: 1 hour*

Just like Chick-Fil-A, but it tastes 10000x better! And the homemade honey mustard is out of this world!

Ingredients:
------------------------------------------------
* 1 pound boneless, skinless chicken breasts, cut into 1-inch chunks
* 1 cup dill pickle juice
* 1 1/2 cups milk, divided
* 1 cup peanut oil
* 1 large egg
* 1 1/4 cups all-purpose flour
* 1 tablespoon confectioners’ sugar
* Kosher salt and freshly ground black pepper, to taste

Directions:
------------------------------------------------------
1. In a large bowl, combine chicken, pickle juice and 1/2 cup milk; marinate for at least 30 minutes. Drain well.
2. Heat peanut oil in a large skillet over medium high heat.
3. In a large bowl, whisk together remaining 1 cup milk and egg. Stir in chicken and gently toss to combine; drain excess milk mixture.
4. In a gallon size Ziploc bag or large bowl, combine chicken, flour and confectioners’ sugar; season with salt and pepper, to taste.
5. Working in batches, add chicken to the skillet and cook until evenly golden and crispy, about 2-3 minutes. Transfer to a paper towel-lined plate.
7. Serve immediately with honey mustard.

Honey Mustard
--------------------------------------------------
* 1/4 cup mayonnaise
* 2 tablespoons honey
* 1 tablespoon mustard
* 2 teaspoons Dijon mustard
* 2 teaspoons freshly squeezed lemon juice

To make the honey mustard, whisk together mayonnaise, honey, mustards and lemon juice in a small bowl; set aside.

Notes:
------------------------------------------------------------------
Adapted from Mandy’s Recipe Box. This delicious recipe brought to you by [Damn Delicious](http://damndelicious.net/2014/07/18/copycat-chick-fil-nuggets/)
