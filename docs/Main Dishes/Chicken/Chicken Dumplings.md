Our Lady of Lourdes Chicken Soup and Dumplings
====================================================

Ingredients
-----------------------------------------------------
###Soup:
* 10 cups Water
* 2 large chicken breasts (cooked and cut up for soup)
* 1 chopped onion
* 1 tsp Salt
* 3/4 tsp Pepper
* 1 Bay Leaf
* 1 1/2 c. Carrots, sliced
* 1 c. Celery, chopped (I used the inner w/leaves)
* 3/4 tsp garlic salt
* 2 Tbsp Chicken Bouillon (I used "Better Than Bouillon" brand)

###Dumplings:
* 2 cups Flour
* 3 tsp Baking Powder
* 1/2 tsp Salt
* 4 Tbsp Cold Butter
* 1 cup Milk
* 3 Tbsp dried Parsley


Directions
-----------------------------------------------
###Soup:
1. Put all in pot and cook these for about an hour. Then add dumplings.

###Dumplings:

1. Stir together the flour, baking powder, salt and parsley. Cut in cold butter until crumbly.

2. Mix in milk with fork. Drop by rounded Tablespoonfuls (or teaspoonful, depending how big you want your dumplings) into the soup.

3. Cover and cook for 10 minutes. Remove cover and carefully flip dumplings. Continue cooking uncovered for 10 minutes.
