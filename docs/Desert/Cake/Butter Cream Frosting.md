Butter Cream Frosting
============================================

Ingredient
----------------------------------------------------------
* 1/2 cup butter 
* 1/2 cup criso
* 1 tsp. vanilla
* 4 cups power sugar
* 2-4 tablespoons cream (whipping cream for fluffy frosting)

Directions
------------------------------------------
1. Blend butter and cream.
2. Add vanilla and power sugar
3. Mix in cream 


Pictures
-----------------------------------------------
![Oringal Recipe](./imgs/ButterCreamFrosting.png "Original Recipe")

Notes
---------------------------------------------------------
* Goes great with [cake!](/Desert/Cake/Chocolate%20Cake/)
