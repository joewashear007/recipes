Five Minute Chocolate Mug Cake
===============================================

Ingredient
----------------------------------------------------------
* 4 Tbsp. Flour
* 4 Tbsp. Sugar
* 2 Tbsp. Cocoa
* 3 Tbsp. Chocolate Chips
* 1 Egg
* 3 Tbsp. Milk
* 3 Tbsp. Oil
* A Splash of Vanilla

Directions
------------------------------------

1. Mix thoroughly in a microwave safe mug.
2. Cook, in microwave, for three minutes ( 1000 watts. )
3. The cake will rise to top of the mug, don't be alarmed. Allow to cool a little and then enjoy!!!
