Crisco Pie Crut
=====================================================

Ingredients
------------------------------------------

| Single   | Double  |              |
|----------|---------|--------------|
| 1 1/3 c. | 2 2/3 c.| Sifted Flour |
| 1/2 t.   | 1 t.    | Salt         |
| 1/2 c.   | 1 c.    | Crisco       |
| 3 T.     | 6 T.    | Water        |

Directions
--------------------------------------------------
1. Mix ingredients
2. Bake at 425° for 6-8 mins.


Pictures
-------------------------------------------------
![Original Recipe](./imgs/CriscoPieCrust.jpg "Original Recipe")

Notes
---------------------------------------------------------
