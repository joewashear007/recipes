Tina's Cheesy Potatoes
==================================================================

Ingredients
----------------------------------------------------------
* 2# bag of Frozen O'brien Potatoes
* 1 Can Cream of Chicken Soup
* 1 pt. (16 oz) Sour Cream
* Stick of Butter
* 10 oz Shredded Cheddar Cheese
* Bread Crumbs

Directions
---------------------------------------
1. Preheat oven to 350
2. Thaw potatoes and salt. 
3. Mix 1.2 the cheese in the potatoes
4. Melt butter, soup, and sour cream in pan over the stove util blended and hot.
5. Pour mixture over the potatoes
6. Top with the rest of the cheese and bread crumb
7. Bake until they look down, about an hour or so

Pictures
-------------------------------------------------
![Original Recipe](./imgs/TinaCheesePotatoes.png "Original Recipe")

Notes
-------------------------------------------------
* Originally From Tina Roth