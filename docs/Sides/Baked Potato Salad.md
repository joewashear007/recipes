Fully Loaded Baked Potato Salad
==================================================================

Ingredients
----------------------------------------------------------
* One 5 pound bag medium Russet Potatoes
* 1 cup sour cream
* 1/2 cup mayonnaise
* 1 package of bacon, cooked and crumbled
* 1 small onion, chopped
* Chives, to taste
* 1 1/2 cups shredded cheddar cheese
* Salt and pepper to taste

Directions
---------------------------------------
1. Wash and poke potatoes. Bake at 350 degrees for about an hour or until fork tender.
2. Cool and cut the potatoes into bite sized chunks with skin.
  * Potatoes can be cool in fridge which helps with the texture and heating the dairy.
  * Not need for hot potatoes
3. Mix mayo and sour cream and add to the potatoes. Add onions, chives, bacon, cheese, Salt and pepper.
4. Top with extra shredded cheese, bacon, and chives, and serve!

-------------------------------------------------
* Originally from [This is My Key West](http://www.thisismykeywest.com/2013/05/16/loaded-baked-potato-salad/_)
* Summer Side Dish
